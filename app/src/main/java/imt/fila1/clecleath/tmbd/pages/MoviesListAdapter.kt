package imt.fila1.clecleath.tmbd.pages

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import imt.fila1.clecleath.tmbd.R
import imt.fila1.clecleath.tmbd.models.MockDataSource
import imt.fila1.clecleath.tmbd.models.Movie

class MoviesListAdapter(val onClick : (Movie) -> Unit) : RecyclerView.Adapter<MoviesListAdapter.ViewHolder>() {
    var movieList : List<Movie> = emptyList()
    @SuppressLint("NotifyDataSetChanged")
    set(value){
        notifyDataSetChanged()
        field = value
    }
    // Classe interne décrivant les vues présentes dans une cellule
// l'objet view est le conteneur généré à partir du layout d'item
// on y trouve les différentes vues présentes dans le layout
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val titleItem : TextView
        val averageVotesItem : TextView
        val dateItem : TextView
        val imageItem : ImageView
        init {
            with(view){
                titleItem = findViewById(R.id.titleItem)
                averageVotesItem = findViewById(R.id.averageVotesItem)
                imageItem = findViewById(R.id.imageItem)
                dateItem = findViewById(R.id.dateItem)
            }
        }
    }
    // cette fonction explique à Android comment créer un objet ViewHolder
// elle est utilisée pour indiquer quel layout décrit les items
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.movie_list_item, parent, false)
        return ViewHolder(view)
    }
    // cette fonction est appelée pour dessiner l'élément dont l'index est "position" dans la liste
// elle va servir a brancher les valeurs d'une entité sur une vue à l'écran
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.titleItem.text = movieList[position].title
        holder.averageVotesItem.text = movieList[position].averageVote.toString()
        holder.dateItem.text = movieList[position].date
        holder.itemView.setOnClickListener{onClick(movieList[position])}

    }
    // cette fonction indique à Android le nombre d'éléments dans la liste
    override fun getItemCount(): Int = movieList.size
}
