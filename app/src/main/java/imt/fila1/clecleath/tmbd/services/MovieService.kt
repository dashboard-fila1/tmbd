package imt.fila1.clecleath.tmbd.services

import imt.fila1.clecleath.tmbd.models.MockDataSource
import imt.fila1.clecleath.tmbd.models.Movie
import imt.fila1.clecleath.tmbd.services.interfaces.MovieInterface
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.http.GET


class MovieService {
    private val dataSource = MockDataSource()
    /*fun getMovieById(id:Int, success: (movie: Movie?) -> Unit, failure: () -> Unit) {
        success(dataSource.movies.find { it.id == id })
    }*/

    fun getMovieById(id:Int, success: (movie: Movie?) -> Unit, failure: () -> Unit) {
        val response =ApiClient.getService().movieDetailById(id).execute()
        if (response.isSuccessful && response.body() != null) {
            success( response.body()!!)
        }

    }

    fun getLastestMovies(
        success: (movies: List<Movie>) -> Unit, failure: () -> Unit) {
        val response =ApiClient.getService().getLastestMovies().execute()
        if (response.isSuccessful && response.body() != null) {
            success( response.body()!!)
        }
    }
}