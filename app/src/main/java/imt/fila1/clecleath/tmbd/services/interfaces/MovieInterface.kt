package imt.fila1.clecleath.tmbd.services.interfaces

import retrofit2.Call
import imt.fila1.clecleath.tmbd.models.Movie
import retrofit2.http.GET
import retrofit2.http.Path

public interface MovieInterface {
       @GET("movie/{id}?api_key=74a4b573182d5dee6b4565cd5f152916")
       fun movieDetailById(@Path("id") id: Int): Call<Movie>
       @GET("movies?api_key=74a4b573182d5dee6b4565cd5f152916")
       fun getLastestMovies() : Call<List<Movie>>
}