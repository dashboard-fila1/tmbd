package imt.fila1.clecleath.tmbd.services

import com.google.gson.GsonBuilder
import imt.fila1.clecleath.tmbd.services.interfaces.MovieInterface
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object ApiClient {

    private lateinit var myService : MovieInterface

    init {
        val gson = GsonBuilder().setLenient().create()

        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        myService = retrofit.create(MovieInterface::class.java)
    }

    fun getService() : MovieInterface {
        return myService
    }
}

