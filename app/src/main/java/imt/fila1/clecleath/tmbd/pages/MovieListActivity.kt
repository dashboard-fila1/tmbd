package imt.fila1.clecleath.tmbd.pages

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import imt.fila1.clecleath.tmbd.R
import imt.fila1.clecleath.tmbd.models.Movie
import imt.fila1.clecleath.tmbd.services.MovieService

class MovieListActivity : AppCompatActivity() {
    private val movieService : MovieService = MovieService()
    lateinit var  moviesListAdapter : MoviesListAdapter;
    private var movieSavedList : List<Movie>? = null;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        movieSavedList = savedInstanceState?.getParcelableArrayList("MOVIES_LIST");
        setContentView(R.layout.activity_movie_list)

    }
    override fun onStart() {
        super.onStart()
        moviesListAdapter = MoviesListAdapter({
            loadDetails(it)
        })
        if(movieSavedList != null){
            moviesListAdapter.movieList = movieSavedList!!;
        }else{
            movieService.getLastestMovies (success = {
                moviesListAdapter.movieList = it
            }, failure = {})
        }

        val moviesList = findViewById<RecyclerView>(R.id.listOfMovie)
        moviesList.adapter = moviesListAdapter
        moviesList.layoutManager = GridLayoutManager(this,3)
    }

    override fun onSaveInstanceState(outState : Bundle){
        outState.putParcelableArrayList("MOVIES_LIST",ArrayList(moviesListAdapter.movieList) )
        super.onSaveInstanceState(outState);

    }

    fun loadDetails(movie : Movie){
        val intent = Intent(this, MovieDetailActivity::class.java)
        intent.putExtra(MovieDetailActivity.INTENT_PARAM_ID, movie.id)
        startActivity(intent)
    }

}