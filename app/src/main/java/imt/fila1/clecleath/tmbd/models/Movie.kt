package imt.fila1.clecleath.tmbd.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
@Parcelize
data class Movie (
    var id : Int,
    var posterUrl:String,
    var title:String,
    var averageVote:Float,
    var votesNumber:Int,
    var overview:String,
    var cast:String,
    var date:String,
    ) : Parcelable