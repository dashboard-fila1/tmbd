package imt.fila1.clecleath.tmbd.models

import java.time.LocalDate
import java.util.Date

class MockDataSource {
    val movies: List<Movie> = listOf(
        Movie(1,"AAAA","WOW LE FILM", 3.4f,4,"C'est l'histoire d'un film qui un jour a été filmé", "Jean PAUL, Paul BERNARD", "21/10/2022"),
        Movie(2,"AAAA","Cinéma : Le film", 5.4f,4,"C'est l'histoire du cinématographe", "JEAN PAUL, HUGH DA", "21/10/2022"),
        Movie(3,"AAAA","YYYYYYYYYYYYYYY", 7.4f,6,"C'est l'histoire d'une mouche", "JEAN PAUL, HUGH DA", "21/10/2022"),
        Movie(4,"AAAA","YYYYYYYYYYYYYYY", 7.4f,6,"C'est l'histoire d'une mouche", "JEAN PAUL, HUGH DA", "21/10/2022"),
        Movie(5,"AAAA","YYYYYYYYYYYYYYY", 7.4f,6,"C'est l'histoire d'une mouche", "JEAN PAUL, HUGH DA", "21/10/2022"),
        Movie(6,"AAAA","YYYYYYYYYYYYYYY", 7.4f,6,"C'est l'histoire d'une mouche", "JEAN PAUL, HUGH DA", "21/10/2022"),
    )
}